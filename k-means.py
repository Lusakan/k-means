import matplotlib.pyplot as plt
import numpy as np
import math
import copy
import sys

def getDist(x1, y1, x2, y2):
    return np.sqrt((x1 - x2)**2 + (y1 - y2)**2)

def getCenterCoordinates(coordinates) :
    x, y = 0, 0
    xArr, yArr = coordinates
    for i in range(0, len(xArr)):
        x += xArr[i] / len(xArr)
        y += yArr[i] / len(yArr)
    return x, y

def printGraph(resDict) :
    colorsList = ['blue', 'green', 'yellow', 'black', 'peru', 'slateblue', 'lightsalmon', 'cornsilk', 'lime', 'cyan', 'magenta', 'purple', 'ping', 'brown', 'grey']
    keys = list(resDict.keys())
    for key in keys:
        plt.scatter(key[0], key[1], c='r')
        plt.scatter(resDict[key][0], resDict[key][1], c=colorsList[keys.index(key)])
    plt.show()

def getRadius(coordinates, centerX, centerY):
    maxDist = 0
    xArr, yArr = coordinates
    for i in range(0, len(xArr)):
        currDist = getDist(xArr[i], yArr[i], centerX, centerY)
        if currDist > maxDist:
                maxDist = currDist
    return maxDist

def getCentroidCoordinates(centroidsAmount, centerX, centerY, radius):
    xArr, yArr = list(), list()
    for i in range(1, centroidsAmount + 1):
            xCoord = radius * math.cos(2 * math.pi * i / centroidsAmount) + centerX
            yCoord = radius * math.sin(2 * math.pi * i / centroidsAmount) + centerY
            xArr.append(xCoord)
            yArr.append(yCoord)
    return xArr, yArr

def equalPoints(firstXCoordinates, firstYCoordinates, secondXCoordinates, secondYCoordinates):
    if (len(secondXCoordinates) == 0): return False
    for i in range(0, len(firstXCoordinates)):
        if ((firstXCoordinates[i] != secondXCoordinates[i]) | (firstYCoordinates[i] != secondYCoordinates[i])):
            return False
    return True

def kMeans(centroidsAmount, coordinates, centerX, centerY, radius, needPrint=False):
    xCentroidCoordinates, yCentroidCoordinates = getCentroidCoordinates(centroidsAmount, centerX, centerY, radius)

    currXCentroids, currYCentroids = xCentroidCoordinates.copy(), yCentroidCoordinates.copy()
    prevXCentroids, prevYCentroids = list(), list()
    xPointCoordinates, yPointCoordinates = coordinates
    pointsAmount = len(xPointCoordinates)
    resDict = {}

    while (not equalPoints(currXCentroids, currYCentroids, prevXCentroids, prevYCentroids)) :
        resDict = {}
        prevXCentroids = copy.deepcopy(currXCentroids)
        prevYCentroids = copy.deepcopy(currYCentroids)

        centroidToPoints = {}
        for i in range(0, centroidsAmount) :
            centroidToPoints[i] = list()

        for i in range(0, pointsAmount):
            nearestCentroid = (-1, sys.maxsize)
            for j in range(0, centroidsAmount):
                distToCentroid = getDist(xPointCoordinates[i], yPointCoordinates[i], currXCentroids[j], currYCentroids[j])
                if (distToCentroid < nearestCentroid[1]) :
                    nearestCentroid = (j, distToCentroid)
            centroidToPoints[nearestCentroid[0]].append(i)
        
        for centroidIndex in centroidToPoints.keys():
            points = centroidToPoints[centroidIndex]
            xLocalPointCoordinates, yLocalPointCoordinates = list(), list()
            for pointId in points:
                 xLocalPointCoordinates.append(xPointCoordinates[pointId])
                 yLocalPointCoordinates.append(yPointCoordinates[pointId])

            newCentroidX, newCentroidY = getCenterCoordinates((xLocalPointCoordinates, yLocalPointCoordinates))
            currXCentroids[centroidIndex] = newCentroidX
            currYCentroids[centroidIndex] = newCentroidY
            resDict[(newCentroidX, newCentroidY)] = (xLocalPointCoordinates, yLocalPointCoordinates)
        
        if (needPrint) :
            printGraph(resDict)

    return resDict

def getOptimalK(centerX, centerY, radius, coordinates): 
    kResults = list()
    for i in range(1, 15):
        resDict = kMeans(i, coordinates, centerX, centerY, radius)
        finalSum = 0
        for key in resDict.keys():
            xPointCoordinates = resDict[key][0]
            yPointCoordinates = resDict[key][1]
            for j in range(0, len(xPointCoordinates)):
                finalSum += math.pow(getDist(xPointCoordinates[j], yPointCoordinates[j], key[0], key[1]), 2)
        kResults.append(finalSum)
    
    minVal = (-1, sys.maxsize)
    for i in range (1, len(kResults) - 1):
        dk = abs(kResults[i] - kResults[i + 1]) / abs(kResults[i - 1] - kResults[i])
        if (dk < minVal[1]):
            minVal = (i, dk)

    plt.figure(figsize=(10, 6))
    plt.plot(kResults, marker='o', linestyle='-', color='b')
    plt.grid(True)
    plt.show()
    return minVal[0]

coordinates = (list(np.random.randint(500, size=100)), list(np.random.randint(500, size=100)))
centerX, centerY = getCenterCoordinates(coordinates)
radius = getRadius(coordinates, centerX, centerY)

optimalK = getOptimalK(centerX, centerY, radius, coordinates)
print(optimalK)
resDict = kMeans(optimalK, coordinates, centerX, centerY, radius, True)

print('Result!!!')
printGraph(resDict)