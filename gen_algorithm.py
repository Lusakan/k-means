import random
import copy
import requests
import openrouteservice

def start_population():
    first_generation = []
    for i in range(5):
        free_indexes = [1, 2, 3, 4, 5, 6]
        generation = []
        for i in range(6):
            random_number = random.randint(0, len(free_indexes) - 1)
            generation.append(free_indexes[random_number])
            free_indexes.pop(random_number)
        first_generation.append(generation)
    return first_generation

def crossover(parents):
    offspring = []
    offspring.append(make_child(parents[0], parents[1]))
    offspring.append(make_child(parents[1], parents[0]))
    offspring.append(make_child(parents[2], parents[3]))
    offspring.append(make_child(parents[3], parents[2]))
    offspring.append(make_child(parents[1], parents[3]))
    offspring.append(make_child(parents[3], parents[1]))
    return offspring

def make_child(parent1, parent2):
    offspring = copy.deepcopy(parent1)
    element_set = set()
    for n in range(0, 3):
        element_set.add(parent1[n])
    for k in range(3, 6):
        for value in parent2:
            if value not in element_set:
                offspring[k] = value
                element_set.add(value)
                break
    return offspring

def mutation(generations):
    number_of_mutations = random.randint(0, len(generations))
    print("Кол-во мутаций: ", number_of_mutations)
    taken_indexes = set()
    for i in range(number_of_mutations):
        number_of_generation = random.randint(1, len(generations))
        while number_of_generation in taken_indexes:
            number_of_generation = random.randint(1, len(generations))
        generation = generations[number_of_generation - 1]
        taken_indexes.add(number_of_generation)
        first_index = random.randint(0, len(generation) - 1)
        second_index = random.randint(0, len(generation) - 1)
        while first_index == second_index:
            first_index = random.randint(0, len(generation) - 1)
            second_index = random.randint(0, len(generation) - 1)
        print("\tМутирование в гене: ", number_of_generation)
        temp = generation[first_index]
        generation[first_index] = generation[second_index]
        generation[second_index] = temp
    return generations

def selection(path_cost, generations):
    selected_generations = []
    total_cost = sum(path_cost)
    costs = list(enumerate(path_cost, start=0))
    for i in range(4):
        selected_index = roulette_wheel_selection(total_cost, costs)
        selected_generations.append(generations[costs[selected_index][0]])
        costs.pop(selected_index)
    return selected_generations

def roulette_wheel_selection(total_cost, path_cost):
    probabilities = [(cost[1]) / total_cost for cost in path_cost]
    swapped_probabilities = [1.0 - prob for prob in probabilities]
    sum_swapped_probabilities = sum(swapped_probabilities)
    normalized_probabilities = [prob / sum_swapped_probabilities for prob in swapped_probabilities]
    r = random.uniform(0, 1)
    cumulative_prob = 0.0

    for i, prob in enumerate(normalized_probabilities):
        cumulative_prob += prob
        if r <= cumulative_prob:
            return i

def evaluation(generations, distance_matrix):
    path_costs = []
    for i in range(len(generations)):
        generation = generations[i]
        path_cost = 0
        for i in range(len(generation) - 1):
            path_cost += distance_matrix[generation[i] - 1][generation[i + 1] - 1]
        path_cost += distance_matrix[generation[len(generation) - 1] - 1][generation[0] - 1]
        path_costs.append(path_cost)
    return path_costs

def getBestPath(generation, paths):
    index = 0;
    for i in range (len(paths)):
        if paths[i] < paths[index]:
            index = i
    return generation[index]

def getCities(client) :
    client = openrouteservice.Client(key = '5b3ce3597851110001cf624876c800dd0e284ec0861585b982ec58d6')
    kazan = client.pelias_structured(country = 'Russia', locality = 'Kazan', region = 'Tatarstan')
    moscow = client.pelias_structured(country = 'Russia', locality = 'Moscow', region = 'Moscow')
    ufa = client.pelias_structured(country = 'Russia', locality = 'Ufa', region = 'Bashkortostan')
    perm = client.pelias_structured(country = 'Russia', locality = 'Perm', region = 'Perm')
    samara = client.pelias_structured(country = 'Russia', locality = 'Samara', region = 'Samara')
    omsk = client.pelias_structured(country = 'Russia', locality = 'Omsk', region = 'Omsk')

    return {
        'Kazan': [kazan['bbox'][0], kazan['bbox'][1]],
        'Moscow': [moscow['bbox'][0], moscow['bbox'][1]],
        'Ufa': [ufa['bbox'][0], ufa['bbox'][1]],
        'Perm': [perm['bbox'][0], perm['bbox'][1]],
        'Samara': [samara['bbox'][0], samara['bbox'][1]],
        'Omsk': [omsk['bbox'][0], omsk['bbox'][1]]
    }

def buildPath(coordinates) :
    base = 'https://classic-maps.openrouteservice.org/directions?n1=69.869892&n2=62.95166&n3=5&b=0&c=0&k1=en-US&k2=km&a='
    for coordinate in coordinates:
        base += str(coordinate[1]) + ',' + str(coordinate[0]) + ','

    return base[0:-1]

if __name__ == '__main__':
    client = openrouteservice.Client(key = '5b3ce3597851110001cf624876c800dd0e284ec0861585b982ec58d6')
    city2Coordinates = getCities(client);
    print(city2Coordinates)
    # city2Coordinates = {'Kazan': [49.12214, 55.78874], 'Moscow': [37.61556, 55.75222], 'Ufa': [55.96779, 54.74306], 'Perm': [56.25017, 58.01046], 'Samara': [50.15, 53.20007], 'Omsk': [73.36859, 54.99244]}
    cities = list(city2Coordinates.keys())
    print(cities)
    matrix = client.distance_matrix(
        locations=[city2Coordinates['Kazan'], city2Coordinates['Moscow'], city2Coordinates['Ufa'], city2Coordinates['Perm'], city2Coordinates['Samara'], city2Coordinates['Omsk']], 
        metrics = ['distance'], 
        units = 'km',
        destinations = [0, 1, 2, 3, 4, 5],
        sources = [0, 1, 2, 3, 4, 5]);
    print(matrix)
    # matrix = {'distances': [
    #     [0.0, 902.58, 522.4, 667.72, 355.89, 1783.43], 
    #     [901.11, 0.0, 1429.47, 1485.23, 1135.48, 2700.6], 
    #     [523.68, 1431.4, 0.0, 464.11, 462.69, 1250.43], 
    #     [667.59, 1485.96, 464.92, 0.0, 850.92, 1316.55], 
    #     [360.35, 1054.99, 463.74, 851.86, 0.0, 1694.26], 
    #     [1783.66, 2702.48, 1251.56, 1322.65, 1694.89, 0.0]
    # ]}

    # 6 городов
    distance_matrix = matrix['distances']

    first_generation = start_population()
    print("Исходные: ", first_generation)
    max_generations = 10
    for i in range(max_generations):
        print("\n")
        path_costs = evaluation(first_generation, distance_matrix)
        print("Длина пути для каждой особи: ", path_costs)
        parents = selection(path_costs, first_generation)
        print("Выбрали эти: ", parents)
        children = crossover(parents)
        print("Новое поколение из выбранных ", children)
        print(" -- МУТАЦИЯ -- ")
        first_generation = mutation(children)
        print("-- РЕЗУЛЬТАТ МУТАЦИИ -- \n\t", first_generation)
    print("\nРЕЗУЛЬТАТ\n", first_generation)
    path_costs = evaluation(first_generation, distance_matrix)
    print("Итоговая длина путей: ", path_costs)
    bestPath = getBestPath(first_generation, path_costs);
    print("Лучший путь: ", bestPath)
    f = lambda x : city2Coordinates[cities[x - 1]]
    res = list(map(f, bestPath))
    print(buildPath(res))